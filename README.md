#  ChatGPT-FreeSites

#### 介绍
本项目用于收集可用的、免费的ChatGPT站点供大家食用。

# Free ChatGPT Site List

**这儿为你准备了众多免费好用的ChatGPT镜像站点**
> 🤭收藏此站点,不迷失；如果**分享站点**或有**站点失效和标注错误**，请[🌺点此🌺](https://gitee.com/xueshi-0802/chat-gpt-free-sites/issues)提交反馈


- ⭐:使用不受限
- 🔑:需要进行**登录**或需要**密码**
- ⛔:有限地使用**次数**或**字数**，需提供key或进行充值进行服务升级
- ❓ :未测试，未进行标注也为未测试

## 站点列表



1. [⭐⭐] https://theb.ai/

2. [🔑⭐] https://poe.com/

3. [⭐⭐] https://chatmate.network/

4. [⭐⭐] https://chat.yqcloud.top/

5. [⭐⭐]https://qa.js.cn/

6. [⭐⭐] https://gpt.xeasy.me/

7. [⭐⭐] https://chat1.xeasy.me/

8. [⭐⭐] https://chat.uue.me/

9. [⭐⭐] https://chat.forchange.cn/

10. http://gitopenchina.gitee.io/gpt

11. http://gitopenchina.gitee.io/freechatgpt

12. https://askbot.club/chatgpt/

13. https://gpt.getshare.net/

14. http://chatai.fyi

15. https://freechatgpt.chat/

16. https://chatplus.app (客户端应用)

17. https://gpt.tool00.com/

18. https://chat.geekr.dev/

19. https://desk.im

20. https://xc.com/

21. https://chat.51buygpt.com/

22. http://chat.apigpt.cn/

23. https://94gpt.com/

24. https://www.teach-anything.com/

25. https://trychatgp.com/

26. https://ai117.com/

27. http://chat.livepo.top/

28. https://freegpt.cc

29. https://ai.ls

30. [🔑] https://www.bz1y.cn/

31. [🔑] https://chat.alpaca-bi.com/

32. [🔑] https://chat.paoying.net/

33. [🔑] https://chat.eaten.fun/

34. [🔑]  https://chat.qingting.work

35. [🔑] https://chat.wxredcover.cn/

36. https://www.askopenai.cn/

37. https://chatgpt-flutter.h7ml.cn/

38. https://www.aitoolgpt.com/

39. https://chatapi.qload.cn/

40. https://gpt.h7ml.cn/

41. https://chat.h7ml.cn/

42. [⛔] https://ai.okmiku.com/chat/

43. [⛔] https://chat-gpt.nikong.cn/

44. [⛔] https://www.tdchat.com/

45. [⛔]  http://gitopenchina.gitee.io/chatgpt

46. [⛔] https://chatforai.com/

47. [⛔] https://ai.okmiku.com/chat/

48. [⛔] https://chatcat.pages.dev/

49. [⛔] https://askgptai.com/

50. [⛔] https://www.chat2ai.cn/

51. [⛔] https://chat.zecoba.cn/

52. [⛔] http://gpt.mxnf.store/

53. [⛔]https://aigcfun.com/

54. [⛔] https://ai.yiios.com/

55. ~~https://freegpt.one/~~

56. ~~https://freechatgpt.lol/~~

57. ~~https://fastgpt.app/~~

58. ~~https://chat.jingran.vip/~~

59. ~~http://itecheasy.com.cn/~~

60. ~~https://chatgpt.ddiu.io/~~

61. ~~https://chat.qingting.work/~~

62. ~~https://chat.aigc-model.com/~~

63. ~~https://chatgpt.poshist.cn/~~

64. ~~https://www.chatsverse.xyz/~~

65. ~~https://ai.v2less.com/ 访问密码：lessismore~~

66. ~~https://chatgpt.h7ml.cn/~~

67. ~~https://chat.tgbot.co/~~

68. ~~https://chat.ninvfeng.xyz/!~~

69. ~~https://talk.xiu.ee/~~

70. ~~https://chat.sheepig.top/~~

71. ~~https://chatgpt.ddiu.me/~~

72. ~~https://chatgpt.lcc8.com/~~

### 妙站

> 下面这些站点也很有趣
0. [gpt小应用市场] https://open-gpt.app/

1. [海豚问答] http://zhimachat.com/

2. [编程] https://www.aicodehelper.com/

3. [综合] https://ai-toolbox.codefuture.top/

4. [虚拟女友] https://chilloutai.com/

5. [文字游戏] https://harry-potter.openai-lab.com

6. [写故事] https://wordstory.streamlit.app/

7. [AI 如来(佛)] https://hotoke.ai/

8. [🔑 技术文档助手] https://docsgpt.arc53.com/

9. [AI 表格助手] https://chatexcel.com/

10. [🔑 AI文秘助手]https://typeset.io/

11. [⛔ AI B站总结] https://b.jimmylv.cn/

12. [BaiDu 文心一言] https://yiyan.baidu.com/welcome

## 欢迎补充

> Gitee 仓库地址: https://gitee.com/xueshi-0802/chat-gpt-free-sites

如果您认为站点可以加⭐、分享你发现的新的站点，或报告失效站点，欢迎提交[issues](https://gitee.com/xueshi-0802/chat-gpt-free-sites/issues)


### 最后更新

> 如果下方时间已经晚于当前时间1h ；请前往[Gitee仓库](https://gitee.com/xueshi-0802/chat-gpt-free-sitest)查看最新内容

>Last synced:BeiJingT 2023-03-21 15:13:40

